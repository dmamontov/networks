
router-net: 192.168.255.0/30
  Broadcast:	192.168.255.3
  Hosts/Net:	2 (оба заняты, один inetRouter, второй centralRouter)

dir-net: 192.168.0.0/28
  Broadcast:	192.168.0.15
  Hosts/Net:	14 (два заняты)

hw-net(main): 192.168.0.32/28
  Broadcast:	192.168.0.47
  Hosts/Net:	14

dev-net(office1): 192.168.2.0/26
  Broadcast:	192.168.2.63
  Hosts/Net:	62 (два заняты)

dev-net(office2): 192.168.1.0/25
  Broadcast:	192.168.1.127
  Hosts/Net:	126 (два заняты)

wifi-net: 192.168.0.64/26
  Broadcast:	192.168.0.127
  Hosts/Net:	62


mgt-net: 192.168.200.0/26
  Broadcast:	192.168.200.63
  Hosts/Net:	62 (3 заняты)


test-net: 192.168.2.64/26
  Broadcast:	192.168.2.127
  Hosts/Net:	62

hw-net (office1): 192.168.2.192/26
  Broadcast:	192.168.2.255
  Hosts/Net:	62

hw-net (office2): 192.168.1.192/26
  Broadcast:	192.168.1.255
  Hosts/Net:	62
