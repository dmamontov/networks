# -*- mode: ruby -*-
# vim: set ft=ruby :

MACHINES = {
  :inetRouter => {
        :box_name => "centos/7",
        #:public => {:ip => '10.10.10.1', :adapter => 1},
        :net => [
                   {ip: '192.168.255.1', adapter: 2, netmask: "255.255.255.252", virtualbox__intnet: "router-net"},
                ]
  },
  :centralRouter => {
        :box_name => "centos/7",
        :net => [
                   {ip: '192.168.255.2', adapter: 2, netmask: "255.255.255.252", virtualbox__intnet: "router-net"},
                   {ip: '192.168.0.1', adapter: 3, netmask: "255.255.255.240", virtualbox__intnet: "dir-net"},
                   {ip: '192.168.0.32', adapter: 4, netmask: "255.255.255.240", virtualbox__intnet: "hw-net"},
                   {ip: '192.168.200.3', adapter: 5, netmask: "255.255.255.192", virtualbox__intnet: "mgt-net"},
                   {ip: '192.168.0.64', adapter: 6, netmask: "255.255.255.192", virtualbox__intnet: "hw-net"},
                ]
  },
  :centralServer => {
        :box_name => "centos/7",
        :net => [
                   {ip: '192.168.0.2', adapter: 2, netmask: "255.255.255.240", virtualbox__intnet: "dir-net"},
#                   {adapter: 3, auto_config: false, virtualbox__intnet: true},
#                   {adapter: 4, auto_config: false, virtualbox__intnet: true},
                ]
  },
  :office1router => {
        :box_name => "centos/7",
        :net => [
                  {ip: '192.168.200.1', adapter: 2, netmask: "255.255.255.192", virtualbox__intnet: "mgt-net"},
                  {ip: '192.168.2.193', adapter: 3, netmask: "255.255.255.192", virtualbox__intnet: "hw-net"},
                  {ip: '192.168.2.1', adapter: 4, netmask: "255.255.255.192", virtualbox__intnet: "dev-net"},
                  {ip: '192.168.2.65', adapter: 5, netmask: "255.255.255.192", virtualbox__intnet: "test-net"},
              ]
  },
  :office1server => {
      :box_name => "centos/7",
      :net => [
                {ip: '192.168.2.2', adapter: 2, netmask: "255.255.255.192", virtualbox__intnet: "dev-net"},
                {adapter: 3, auto_config: false, virtualbox__intnet: true},
              ]
  },
  :office2router => {
    :box_name => "centos/7",
    :net => [
                  {ip: '192.168.200.2', adapter: 2, netmask: "255.255.255.192", virtualbox__intnet: "mgt-net"},
                  {ip: '192.168.1.193', adapter: 3, netmask: "255.255.255.128", virtualbox__intnet: "hw-net"},
                  {ip: '192.168.1.1', adapter: 4, netmask: "255.255.255.128", virtualbox__intnet: "dev-net"},
                  {ip: '192.168.1.128', adapter: 5, netmask: "255.255.255.192", virtualbox__intnet: "test-net"},
            ]
  },
  :office2server => {
    :box_name => "centos/7",
    :net => [
             {ip: '192.168.1.2', adapter: 2, netmask: "255.255.255.128", virtualbox__intnet: "dev-net"},
             {adapter: 3, auto_config: false, virtualbox__intnet: true},
            ]
    },
}

Vagrant.configure("2") do |config|
  MACHINES.each do |boxname, boxconfig|

    config.vm.define boxname do |box|

        box.vm.box = boxconfig[:box_name]
        box.vm.host_name = boxname.to_s
        box.vm.provider "virtualbox"

        boxconfig[:net].each do |ipconf|
          box.vm.network "private_network", ipconf
        end

        if boxconfig.key?(:public)
          box.vm.network "public_network", boxconfig[:public]
        end

        box.vm.provision "shell", inline: <<-SHELL
            	echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD1j1A0g7ebdghBy2UX+baArSx9jzYGTS5HwqyKEniRZ49wc/P20v5HLboCW+pzf3W8GYN3wAN2yTgxczydBO30bXAFhmAwsv/WI43kPRHLlgswSTV7KZcChqc//IsLi74LDY2kxpxcfQZG4uD3ZijsATbyZYO1atBcG5o2Ufobu0FQpSAh9F7eR0mtXzMa2XA2Mt8OlWkzmM3Oz/ORdAPqt9AMLIDk5jiCsBF8qEkLvDHtgU2MwWE6JywbdEYwQ+c6cB17LnJHaoNjOnQ/6z/YLV0TBnXeDO+SwW5dm36IPC9bulBqy4q/mo9cV3Ml3qOT41HsA2fRG8FxXpMum5aD OpenSSH-rsa-import-101417" >> /home/vagrant/.ssh/authorized_keys
        #  mkdir -p ~root/.ssh
        #        cp ~vagrant/.ssh/auth* ~root/.ssh
        SHELL

        case boxname.to_s
        when "inetRouter"
          box.vm.provision "shell", run: "always", inline: <<-SHELL
            sysctl net.ipv4.conf.all.forwarding=1
            sysctl net.ipv4.ip_forward=1
            iptables -t nat -A POSTROUTING ! -d 192.168.0.0/16 -o eth0 -j MASQUERADE
            ip r add 192.168.0.0/28 via 192.168.255.2
          SHELL

        when "centralRouter"
          box.vm.provision "shell", run: "always", inline: <<-SHELL
            sysctl net.ipv4.conf.all.forwarding=1
            sysctl net.ipv4.ip_forward=1
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
            echo "GATEWAY=192.168.255.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
            systemctl restart network

            ip route add default via 192.168.255.1
            ip r add 192.168.1.0/25 via 192.168.200.2
            ip r add 192.168.2.0/25 via 192.168.200.1

            iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE
            iptables -A FORWARD -i eth1 -o eth2 -m state --state RELATED,ESTABLISHED -j ACCEPT
            iptables -A FORWARD -i eth2 -o eth1 -j ACCEPT
            #ip link set dev eth0 down
          SHELL

        when "centralServer"
          box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
            echo "GATEWAY=192.168.0.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
            systemctl restart network

            ip route add default via 192.168.0.1

            #ip link set dev eth0 down
          SHELL

      when "office2router"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
          sysctl net.ipv4.conf.all.forwarding=1
          sysctl net.ipv4.ip_forward=1
          echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
          echo "GATEWAY=192.168.200.3" >> /etc/sysconfig/network-scripts/ifcfg-eth1
          systemctl restart network

          ip route add default via 192.168.200.3

          iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE
          iptables -A FORWARD -i eth1 -o eth2 -m state --state RELATED,ESTABLISHED -j ACCEPT
          iptables -A FORWARD -i eth2 -o eth1 -j ACCEPT
          #ip link set dev eth0 down

        SHELL

      when "office2server"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
          echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
          echo "GATEWAY=192.168.1.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
          systemctl restart network

          ip route add default via 192.168.1.1
          #ip link set dev eth0 down

        SHELL
      when "office1router"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
          sysctl net.ipv4.conf.all.forwarding=1
          sysctl net.ipv4.ip_forward=1
          echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
          echo "GATEWAY=192.168.200.3" >> /etc/sysconfig/network-scripts/ifcfg-eth1
          systemctl restart network

          ip route add default via 192.168.200.3

          iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE
          iptables -A FORWARD -i eth1 -o eth2 -m state --state RELATED,ESTABLISHED -j ACCEPT
          iptables -A FORWARD -i eth2 -o eth1 -j ACCEPT

          #ip link set dev eth0 down

        SHELL
      when "office1server"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
          echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
          echo "GATEWAY=192.168.2.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
          systemctl restart network

          ip r add default via 192.168.2.1
          #ip link set dev eth0 down

        SHELL
      end
      end
  end
end
